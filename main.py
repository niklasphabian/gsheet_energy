'''
Created on Apr 13, 2016

@author: EIFERKITEDU\griessbaum
'''


import gspread
import pandas
import datetime
import pickle
import plots 

from oauth2client.service_account import ServiceAccountCredentials

def gs2pd():
    scope = 'https://spreadsheets.google.com/feeds'
    credentials = ServiceAccountCredentials.from_json_keyfile_name('nphabian-ca2fa33aeb02.json', scope)
    gc = gspread.authorize(credentials)
    wb = gc.open_by_key('1Lnxohjcm1AkseEFZB8RZAcQ-Wl3olf6Hfgj-BBRTD-M')
    ws = wb.worksheet('Energy')
    raw_time_stamps = ws.col_values(2)[36:-1]
    raw_energy = ws.col_values(4)[36:-1]
    time_stamps = []
    energy_values = []
    for idx in range(len(raw_time_stamps)) :
        time_stamp = raw_time_stamps[idx]
        energy = raw_energy[idx]
        if len(time_stamp) > 0 :
            time_stamps.append(datetime.datetime.strptime(time_stamp, '%Y-%m-%d'))
            energy_values.append(float(energy.replace(' ','')))    
    df = pandas.DataFrame(data={'TimeStamp': time_stamps, 'Energy': energy_values})
    with open('dump.pickle', 'wb') as dump :
        pickle.dump(df, dump)
    return df


class Energy_df(pandas.DataFrame):

    def add_avg_power(self):
        self['delta_t'] = self.diff()['TimeStamp']
        delta_t = self['delta_t'].dt.total_seconds()
        delta_e = self['Energy'].diff()
        self['avg_power'] = delta_e/delta_t * 3600
        
    def add_weekday(self):
        self['weekday'] = self['TimeStamp'].dt.dayofweek
        
    def add_daily_energy(self):
        self['daily_energy'] = self['avg_power']*24
        
    

def analyze():
    with open('dump.pickle', 'rb') as dump :
        df = pickle.load(dump)
    
    df = Energy_df(df)
    df.add_avg_power()
    df.add_weekday()
    df.add_daily_energy()
    
    
    df.set_index(keys='TimeStamp', inplace=True)
    
    
    plot = plots.DayPlot()
    plot.plot_df(df.groupby('weekday').mean()['daily_energy'])
    plot.show()

    

def main():
    #gs2pd()
    analyze()
    
    
    
main()