'''
Created on Apr 13, 2016

@author: EIFERKITEDU\griessbaum
'''

import matplotlib.pyplot as plt
import numpy

class BarPlot():
    def __init__(self):
        fig, self.ax = plt.subplots(nrows=1, ncols=1, figsize=(15, 8))
        self.width = 0.3
        self.color = 'b'
        self.setupPlot()
        
    def plot(self, yValues, legend=None):
        self.offset = 0
        self.ax.bar(self.xVals+self.offset, yValues, self.width, color=self.color, align='center', label=legend)
        self.ax.legend()
        
    def plot_df(self, df):
        self.plot(df.values)
        
    def show(self):
        self.ax.grid('on')
        plt.show()
           
        
class DayPlot(BarPlot):
    
    def setupPlot(self):
        self.xVals = numpy.array([1,2,3,4,5,6,7])
        self.xticks = ['Monday','Tuesday','Wednesday','Thursday', 'Friday', 'Saturday', 'Sunday']
        self.ax.set_xticks(self.xVals, minor=False)
        self.ax.set_xticklabels(self.xticks, fontdict=None, minor=False)
        self.ax.set_xlim(0, 8)
        self.ax.set_xlabel('Weekdays')
        self.ax.set_ylabel('Average daily energy Demand in kWh')
        
    